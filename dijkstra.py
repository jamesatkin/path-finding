import numpy as np
import sys
import math
import tkinter
import matplotlib.pyplot as plt
import random
from tqdm import tqdm
import time

class Di_Node:
    """Data class for nodes in the Dijkstra's algoritm
    """

    def __init__(self, position, grid):
        self.position = position
        self.neighbours = []
        WALKABLE_VAL = 0
        
        for new_position in [(0, -1), (0, 1), (-1, 0), (1, 0), (-1, -1), (-1, 1), (1, -1), (1, 1)]:
            
            # Creating neighbour nodes
            node_position = (self.position[0] + new_position[0], self.position[1] + new_position[1])
            if node_position[0] > (len(grid) - 1) or node_position[0] < 0 or node_position[1] > (len(grid[len(grid)-1]) -1) or node_position[1] < 0:
                continue

            # Checking if child position is a walkable position
            if grid[node_position[0]][node_position[1]] != WALKABLE_VAL:
                continue

            self.neighbours.append((node_position[0], node_position[1]))

    def __eq__(self, other):
        if other is None:
            return False 
        else:
            return self.position == other.position


def calc_euclidean_distance(i, j):
    """Finds the euclidean distance between the two points provided

    Args:
        i (tuple): coordinate
        j (tuple): coordinate

    Returns:
        float: The euclidean distance between i and j
    """
    return math.sqrt(((i[0] - j[0]) ** 2) + ((i[1] + j[1]) ** 2))

def calc_cost_of_path(path):
    """Calculates the total euclidean distance of a series of coordinates

    Args:
        path (list): A list of x,y coordinates

    Returns:
        float: The total euclidean distance from the start of the path to the end
    """

    total_cost = 0
    prev_node = path[0]
    for node in path[1:]:
        total_cost += calc_euclidean_distance(prev_node, node)
        prev_node = node

    return total_cost

def dijkstras(start, grid, end, storage_list):
    """Runs the dijkstras algorithm

    Args:
        start (tuple): The starting point of the path
        grid (list): The grid containing information about the environment
        end (tuple): The end coordinate of the path

    Returns:
        list: Returns the path between the start and end coordinate. The tuples within the list are reversed
        allowing them to be used directly with the grid data structure
    """
    nodes = []
    dist = []
    previous = []

    # Creating the necessary lists to keep track of the best nodes
    for y, row in enumerate(grid):
        node_row = []
        dist_row = []
        prev_row = []
        for x, col in enumerate(row):
            node_row.append(Di_Node((y, x), grid))
            dist_row.append(sys.maxsize)
            prev_row.append(None)

        nodes.append(node_row.copy())
        dist.append(dist_row.copy())
        previous.append(prev_row.copy())

    dist[start[1]][start[0]] = 0
    nodes = np.array(nodes)
    unoptim = nodes.flatten()
    unoptim = unoptim.tolist()

    # Iterating over the list of unoptimised points until we have created a graph containing all points
    # and their optimised costs
    while len(unoptim) != 0:

        # Finding node with smallest distance to start
        min_dist = sys.maxsize
        min_node = None
        for node in unoptim:
            if dist[node.position[0]][node.position[1]] <= min_dist:
                min_node = node
                min_dist = dist[node.position[0]][node.position[1]]

        if min_node.position == (end[1], end[0]):
            break

        unoptim.remove(min_node)
        storage_list.append(min_node.position)
        min_node_pos_swap = (min_node.position[1], min_node.position[0])
        
        # Iterating over the current nodes neighbours to calculate the cost to go to any of those
        # nodes
        for neighbour in min_node.neighbours:
            should_continue = True

            for node in unoptim:
                if neighbour == node.position:
                    should_continue = False
                    break
                
            if should_continue: continue
            
            alt = dist[min_node.position[0]][min_node.position[1]] + calc_euclidean_distance(min_node_pos_swap, (neighbour[1], neighbour[0]))
            if alt < dist[neighbour[0]][neighbour[1]]:
                dist[neighbour[0]][neighbour[1]] = alt
                previous[neighbour[0]][neighbour[1]] = min_node

    # Forming and returning the final path in reversed order
    path = [(end[0], end[1])]
    current_node = previous[end[1]][end[0]]

    while current_node != None:
        path.append((current_node.position[1], current_node.position[0]))
        current_node = previous[current_node.position[0]][current_node.position[1]]

    # If the path only contains 1 node then there is no path to that target node
    if len(path) == 1:
        return None

    return path[::-1]

def display_dijkstras(grid, storage_list, path):
    """Displays the playback of the provided Dijkstra's path finding algorithm

    Args:
        grid (list): The maze in which the path was planned
        storage_list (list): A list of coordinates corresponding to the order in which the 
        grid's coordinates were optimised
        path (list): The final path returned by the path finding algorithm
    """
    wall_positions = []
    WALL_VALUE = 1

    # Finds the positions of walls in the provided grid
    for i, row in enumerate(grid):
        for j in range(len(row)):
            if row[j] == WALL_VALUE:
                wall_positions.append((j, i))
    
    current_opt_node = storage_list[0]
    wall_positions = np.array(wall_positions)
    
    try:
        plt.ion()
        fig, ax = plt.subplots()
        fig.canvas.set_window_title("Dijkstra's Algorithm")
        ax.set_xlim([0, len(grid[0]) - 1])
        ax.set_ylim([0, len(grid) - 1])
        fig.suptitle("Dijkstra's Algorithm")

        # Setting up scatter plots
        opt_sc = ax.scatter(current_opt_node[1], current_opt_node[0], c = 'g', marker="o", label="optimised")
        walls_sc = ax.scatter(wall_positions[:, 0], wall_positions[:, 1], c = 'r', marker="o", label="walls")
        plt.legend(loc='upper left')
        plt.draw()
        plt.pause(0.01)
        
        all_steps = []

        for step in tqdm(storage_list[1:]):
            all_steps.append(step)
            current_steps = np.array(all_steps)
            opt_sc.set_offsets(np.c_[current_steps[:, 1], current_steps[:, 0]])
            fig.canvas.draw_idle()
            plt.pause(0.01)

        # Plotting path found by the path finding algorithm
        ax.set_title(("Total path cost: %d" %calc_cost_of_path(path)))
        np_path = np.array(path)
        path_lines, = ax.plot(np_path[:, 1], np_path[:, 0], c = "b")
        path_sc = ax.scatter(np_path[:, 1], np_path[:, 0], c = "b", marker = "o")
        plt.show(block=True)
        
    except tkinter.TclError:
        exit(0)

def run_dijkstras(maze):
    """Runs Dijkstra's algorithm on the provided maze looking for a path from the bottom left
    corner to the top right corner

    Args:
        maze (list): A two dimensional list consisting of 0s and 1s, 1s indicating walls and 0s
        indicating walkable terrain

    Returns:
        int: Returns 0 on success, -1 on failure to find a path
    """
    start = (0, 0)
    end = (len(maze[0]) - 1, len(maze) - 1)
    storage = []
    FAIL = -1
    SUCCESS = 0

    print("Running Dijkstra's path finding...")
    start_time = time.time()
    path = dijkstras(start, maze, end, storage)
    end_time = time.time()
    
    if path is None:
        print("Path to desired destination could not be found!")
        return FAIL
    
    print("Finished! - Time Taken: %.3f seconds" % (end_time-start_time))
    print("Showing playback...")
    display_dijkstras(maze, storage, path)
    return SUCCESS
