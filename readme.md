# Path Finding Algorithms
This project was inspired by the animations shown on the wikipedia pages for the A* and Dijkstra's path finding algorithms. The project aims to provide a clean way of showing how the A* and Dijkstra's algorithms work through simple code and animations.

## How to run the program
In order to run the program simply run the run_path_finders.py script. I advise running it initially with the -h flag to get the full-list of commands. The script allows each path finding algorithm to be run individually or both run sequentially on the same randomly generated maze, thus allowing for performance comparisons to be made.

## Extensions
This project is certainly extendable as there are more path finding algorithms which can be added and animated. Overtime I will try to add more algorithms when I can but I encourage others to do so as well.

## A* Example
<img src="https://media.giphy.com/media/gIMe0SlPF056JZWN0W/giphy.gif" width="479" height="377" />