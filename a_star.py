import math
import matplotlib.pyplot as plt
import tkinter
import numpy as np
import random
from tqdm import tqdm
import time

class Node():
    """A data class for nodes in the A* algorithm
    """

    def __init__(self, parent=None, position=None):
        self.parent = parent
        self.position = position
        self.g = 0
        self.h = 0
        self.f = 0

    def __eq__(self, other):
        return self.position == other.position

    def __str__(self):
        return self.position.__str__()

def astar(grid, start, end, storage_list):
    """Runs the A* algorithm on the provided grid and the start and end points

    Args:
        grid (list): A list of lists of integers consisting of 0s and 1s, 1s indicating
        the presence of a wall
        start (tuple): A tuple of integers corresponding to the start position in the map
        end (tuple): A tuple of integers corresponding to the end position of the path in the map
        storage_list (list): An empty list which is filled with each stage of the algorithm allowing
        its decisions to be analysed and displayed

    Returns:
        list: Returns a list of tuples corresponding to the coordinates of each point in the path
    """

    # Creating start and end node
    start_node = Node(position=start)
    end_node = Node(position=end)

    open_list = [start_node]
    closed_list = []
    WALKABLE_VAL = 0

    # Looping until we find the end node
    while len(open_list) > 0:
        
        current_node = open_list[0]
        current_index = 0

        # Finding the node with the lowest f value
        for item in open_list:
            if item.f < current_node.f:
                current_node = item

        # removing node with lowest f value from open list and adding it to closed list
        open_list.remove(current_node)
        closed_list.append(current_node)

        # The end node has been found, backtracking to get the path we just followed
        if current_node == end_node:
            path = []
            current = current_node
            while current is not None:
                path.append(current.position)
                current = current.parent
            return path[::-1]

        children = []
        
        # Iterating over adjacent positions (children of the parent)
        for new_position in [(0, -1), (0, 1), (-1, 0), (1, 0), (-1, -1), (-1, 1), (1, -1), (1, 1)]:
            
            node_position = (current_node.position[0] + new_position[0], current_node.position[1] + new_position[1])
            
            # Checking if child position is within the boundaries of our grid
            if node_position[0] > (len(grid[0]) - 1) or node_position[0] < 0 or node_position[1] > (len(grid) - 1) or node_position[1] < 0:
                continue

            # Checking if child position is a walkable position
            if grid[node_position[1]][node_position[0]] != WALKABLE_VAL:
                continue
                
            # Checking if child is already in the closed list
            if Node(current_node, node_position) in closed_list:
                continue
            
            current_child = Node(current_node, node_position)
            
            # Setting up the f,g and h values for the child
            current_child.g = current_node.g + 1
            # Calculating euclidean distance between child and end nodes
            current_child.h = math.sqrt(((current_child.position[0] - end_node.position[0]) ** 2) + ((current_child.position[1] - end_node.position[1]) ** 2))
            current_child.f = current_child.g + current_child.h

            # Checking if the child node is already present in the open node
            should_continue = False
            for open_node in open_list:
                # If child in open list but has a further g do not explore
                if current_child == open_node and current_child.g >= open_node.g:
                    should_continue = True
                    break
            
            if should_continue: continue

            # Adding the child to the open list
            open_list.append(current_child)

        storage_list.append((open_list.copy(), closed_list.copy()))

def calc_euclidean_distance(i, j):
    """Finds the euclidean distance between the two points provided

    Args:
        i (tuple): coordinate
        j (tuple): coordinate

    Returns:
        float: The euclidean distance between i and j
    """
    return math.sqrt(((i[0] - j[0]) ** 2) + ((i[1] + j[1]) ** 2))

def calc_cost_of_path(path):
    """Calculates the total euclidean distance of a series of coordinates

    Args:
        path (list): A list of x,y coordinates

    Returns:
        float: The total euclidean distance from the start of the path to the end
    """
    total_cost = 0
    prev_node = path[0]
    for node in path[1:]:
        total_cost += calc_euclidean_distance(prev_node, node)
        prev_node = node

    return total_cost

def display(data, grid, path):
    """Displays the playback of the provided path finding algorithm

    Args:
        data (list): A list of lists of the open and closed points in the A* algorithm
        grid (list): The maze in which the path was planned
        path (list): The final path returned by the path finding algorithm
    """
    wall_positions = []
    WALL_VALUE = 1

    # Finds the positions of walls in the provided grid
    for i, row in enumerate(grid):
        for j in range(len(row)):
            if row[j] == WALL_VALUE:
                wall_positions.append((j, i))

    # Getting the x and y data from the positions in the open, closed and wall positions list
    open_list, closed_list = data[0][0], data[0][1]
    open_x, open_y = [], []
    for i in open_list:
        open_x.append(i.position[0])
        open_y.append(i.position[1])

    closed_x, closed_y = [], []
    for i in closed_list:
        closed_x.append(i.position[0])
        closed_y.append(i.position[1])
    
    wall_positions = np.array(wall_positions)

    try:
        plt.ion()
        fig, ax = plt.subplots()
        fig.canvas.set_window_title("A* Algorithm")
        ax.set_xlim([0, len(grid[0]) - 1])
        ax.set_ylim([0, len(grid) - 1])
        fig.suptitle("A* Algorithm")

        # Setting up scatter plots
        open_sc = ax.scatter(open_x, open_y, c = 'g', marker="o", label="open")
        closed_sc = ax.scatter(closed_x, closed_y, c = 'k', marker="o", label="closed")
        walls_sc = ax.scatter(wall_positions[:, 0], wall_positions[:, 1], c = 'r', marker="o", label="walls")
        plt.legend(loc='upper left')
        plt.draw()
        plt.pause(0.1)

        # Iterating over each step of the algorithm, showing what positions are being considered and
        # what are not
        for step in tqdm(data[1:]):
            open_list = step[0]
            closed_list = step[1]
            open_x, open_y, closed_x, closed_y = [], [], [], []

            for i in open_list:
                open_x.append(i.position[0])
                open_y.append(i.position[1])

            for i in closed_list:
                closed_x.append(i.position[0])
                closed_y.append(i.position[1])

            open_sc.set_offsets(np.c_[open_x, open_y])
            closed_sc.set_offsets(np.c_[closed_x, closed_y])
            fig.canvas.draw_idle()
            plt.pause(0.1)

        # Plotting path found by the path finding algorithm
        ax.set_title(("Total path cost: %d" %calc_cost_of_path(path)))
        np_path = np.array(path)
        path_lines, = ax.plot(np_path[:, 0], np_path[:, 1], c = "b")
        path_sc = ax.scatter(np_path[:, 0], np_path[:, 1], c = "b", marker = "o")
        plt.show(block=True)

    except tkinter.TclError:
        exit(0)

def run_astar(maze):
    """Runs the A* algorithm on the provided maze looking for a path from the bottom left
    corner to the top right corner

    Args:
        maze (list): A two dimensional list consisting of 0s and 1s, 1s indicating walls and 0s
        indicating walkable terrain

    Returns:
        int: Returns 0 on success, -1 on failure to find a path
    """
    path = None
    start = (0, 0)
    end = (len(maze[0]) - 1, len(maze) - 1)
    progress = []
    FAIL = -1
    SUCCESS = 0

    print("Running A* path finding...")
    start_time = time.time()
    path = astar(maze, start, end, progress)
    end_time = time.time()

    if path is None:
        print("Path to desired destination could not be found!")
        return FAIL
        
    print("Finished! - Time Taken: %.3f seconds" % (end_time-start_time))
    print("Showing playback...")
    display(progress, maze, path)
    return SUCCESS
